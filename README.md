Script permettant de faciliter la création de client pour OpenVPN
==========================

Ce script est en rapport avec ce tutoriel : http://blog.remyj.fr/linux/configurer-serveur-vpn-openvpn/

1 . Installation
---------------------
Ce script est dépendant du logiciel [mutt](http://www.mutt.org/).
Le fichier creer_client.sh se place dans le répertoire de votre choix et le template est à placer par défaut dans le dossier de configuration d'OpenVPN (/etc/openvpn)

2 . Configuration
-----------------------
Modifier les variables hostname et port suivant votre IP/Nom d'hote et port de votre serveur VPN dans le fichier creer_client.sh
Si vous choisisez de changer l'emplacement des fichiers, modifiez les variables correspondantes aux chemins d'accès des fichiers

3 . Utilisation
------------------
Exécuter le script avec comme paramètres le nom du certificat ainsi que l'email.
La configuration, comprenant tous les certificats et clés necessaires sera envoyé par mail à l'adresse indiqué
Exemple :

``./creer_client.sh mon_client exemple@domaine.com``
