#!/bin/bash
EASY_RSA_PATH="/etc/openvpn/easy-rsa/" # Chemin du dossier d'easy-rsa
TEMPLATE="/etc/openvpn/template_client.ovpn" # Chemin du template
HOSTNAME='vpn.domaine.com' #Ip ou domaine du serveur
PORT='443' # Port du serveur
if [ $1 ] && [ $2 ] # Si le nom et l'adresse du client sont renseignés, on execute le script
then
	cd $EASY_RSA_PATH
        if [ ! -e "keys/$1.crt" ] # Si le certificat du client n'existe pas, on le crée
        then
		source vars
		./build-key $1
	fi
	cp $TEMPLATE '/tmp/vpn.ovpn' # Copie du template de la configuration client pour la modifier
    sed -e "/ca_certificate/ {
	r keys/ca.crt
	d }" -e "s/ipadress/$HOSTNAME/g" -e "s/port/$PORT/g" -e "/tls_auth_key/ {
	r keys/ta.key
	d }" -e "/client_certificate/ {
	r keys/$1.crt
	d }" -e "/client_private_key/ {
	r keys/$1.key
	d }" -i '/tmp/vpn.ovpn' #On inclue dans la config les variables
	echo "Voici votre configuration du VPN. Cette configuration est à placer dans le dossier config dans le répertoire d'installation d'OpenVPN" | mutt -a "/tmp/vpn.ovpn" -s "Votre configuration VPN" -- $2 # On envoie la configuration par mail
	rm '/tmp/vpn.ovpn' # On supprime la configuration
else # Sinon, on affiche l'utilisatation de la commande
	echo "Usage : $0 client email"
fi
